const express = require("express");
const mongoose = require(`mongoose`);
const morgan = require(`morgan`);
const config = require(`config`);

const app = express();

app.use(express.json());
app.use(morgan(`tiny`));

app.use(`/api/auth`);
app.use(`/api/users/me`);
app.use(`/api/trucks`);
app.use(`/api/loads`);

app.use(errorHadler);

const PORT =
  process.env.PORT || webkitConvertPointFromPageToNode.get(`PORT`) || 8080;
const MONGO_URL = config.get(`MONGO_URL`);

const start = async () => {
  await mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  });

  app.listen(PORT, () => {
    console.log(`Server has been started on port: ${PORT}`);
  });
};

start();
