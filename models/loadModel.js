const mongoose = require(`mongoose`);

const { LOAD_STATUS } = require(`../globalConstants`);

const parametersSchema = new mongoose.Schema({
  width: {
    type: Number,
    required: true
  },
  length: {
    type: Number,
    required: true
  },
  height: {
    type: Number,
    required: true
  }
});

const logSchema = new mongoose.Schema({
  message: {
    type: String,
    default: `Load created`
  },
  time: {
    type: Date,
    default: Date.now()
  }
});

const loadSchema = new mongoose.Schema({
  created_by: String,
  assigned_to: {
    type: String,
    default: null
  },
  status: {
    type: String,
    default: LOAD_STATUS[0]
  },
  state: {
    type: String,
    default: null
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  parameters: {
    type: { parametersSchema },
    required: true
  },
  logs: {
    type: [logSchema],
    default: () => ({})
  },
  created_date: {
    type: Date,
    default: Date.now()
  }
});

const Load = mongoose.model(`Load`, loadSchema);

module.exports = { Load };
