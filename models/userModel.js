const mongoose = require(`mongoose`);

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    default: `User name`
  },
  lastName: {
    type: String,
    default: `User surname`
  },
  role: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  createdDate: {
    type: Date,
    default: Date.now()
  }
});

const User = mongoose.model(`User`, userSchema);

module.exports = { User };
